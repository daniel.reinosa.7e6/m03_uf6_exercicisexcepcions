import java.io.IOException
import kotlin.io.path.Path
import kotlin.io.path.readText

val file = Path("logFile.txt")
fun main(){
    try {
        val reader = file.readText()
        println(reader)
    }catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida:")
    }

}
