import java.lang.NumberFormatException
import java.util.*

fun main() {
    val lista: MutableList<String> = mutableListOf("A", "B", "C")
    try {
        lista.add("D")
        val element = lista[4]
        println("El valor de l'element és: $element")
    } catch (e: NoSuchElementException) {
        println("S'ha produït una excepció de tipus NoSuchElementException.")
    }
    try {
        lista.removeAt(2)
        println("La llista després de l'eliminació és: $lista")
        lista.remove("B")
        println("La llista després de l'eliminació és: $lista")
    } catch (e: UnsupportedOperationException) {
        println("S'ha produït una excepció de tipus UnsupportedOperationException.")
    }
}